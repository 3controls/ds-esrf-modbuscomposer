//+=============================================================================
//
// file :         ModbusComposerThread.cpp
//
//
// description :  This class is used for ModbusComposer cache thread
//
// project :      ModbusComposer TANGO Device Server 
//
// $Author: pons$
//
// $Revision: $
//
// $Log: $
//
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
#pragma once
#include "stdafx.h"
namespace ModbusComposer_ns
{
class ModbusComposerThread;
}


#include "ModbusComposerThread.h"
#include <Windows.h>
#include <WinSock2.h>
#include <stdint.h>
#define WIN32_LEAN_AND_MEAN


//=============================================================================
// GETTIMEOFDAY() function
// Windows does not contain gettimeofday function, so this is the solution for that problem.
// Function that waits in microsecond-period of time.
/*typedef struct timeval {
    long tv_sec;
    long tv_usec;
} timeval;*/

int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
    // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
    // This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
    // until 00:00:00 January 1, 1970 
    static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

    SYSTEMTIME  system_time;
    FILETIME    file_time;
    uint64_t    time;

    GetSystemTime( &system_time );
    SystemTimeToFileTime( &system_time, &file_time );
    time =  ((uint64_t)file_time.dwLowDateTime )      ;
    time += ((uint64_t)file_time.dwHighDateTime) << 32;

    tp->tv_sec  = (long) ((time - EPOCH) / 10000000L);
    tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
    return 0;
}

//====================================================================================
// USLEEP() function
// Windows does not contain usleep function, so this is the solution for that problem.
// Function that waits in microsecond-period of time.
//====================================================================================
void usleep(__int64 usec) 
{ 
    HANDLE timer; 
    LARGE_INTEGER ft; 

    ft.QuadPart = -(10*usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL); 
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0); 
    WaitForSingleObject(timer, INFINITE); 
    CloseHandle(timer); 
}

namespace ModbusComposer_ns
{



  // Constructor:
  ModbusComposerThread::ModbusComposerThread(ModbusComposer *modbuscomposer, omni_mutex &m):
    Tango::LogAdapter(modbuscomposer), mutex(m), ds(modbuscomposer)
  {
    INFO_STREAM << "ModbusComposerThread::ModbusComposerThread(): entering." << endl;
    tickStart = -1;
    start_undetached();
    readError = "Cache not yet initialised";
    readOK = false;
  }

  // ----------------------------------------------------------------------------------------

  void *ModbusComposerThread::run_undetached(void *arg)
  {
      
    while(ds->useCache || ds->useCoilCache) {
          
      time_t t0 = get_ticks();

      Tango::DeviceData argout;
      Tango::DeviceData argin;
      vector<short> input;
      vector<short> outputReg;
      vector<short> outputCoil;

      try {


	if( ds->useCache ) {
          input.push_back(ds->cacheStartAddress);
          input.push_back(ds->cacheLength);
          argin << input;
          argout = ds->modbusDS->command_inout(ds->defaultReadCommand.c_str(),argin);
          argout >> outputReg;
        }

	if( ds->useCoilCache ) {
          input.push_back(ds->cacheCoilStartAddress);
          input.push_back(ds->cacheCoilLength);
          argin << input;
          argout = ds->modbusDS->command_inout("ReadMultipleCoilsStatus",argin);
          argout >> outputCoil;
        }

	readOK = true;

      } catch( Tango::DevFailed &e ) {

	readError = e.errors[0].desc;
	readOK = false;
	
      }

      // Update share part
      mutex.lock();
      ds->cacheOK =  readOK;
      if( readOK ) {
        ds->cacheBuffer = outputReg;
        ds->cacheCoilBuffer = outputCoil;
	ds->cacheError = "";
      } else {
	ds->cacheError = readError;
	ds->cacheBuffer.clear();
	ds->cacheCoilBuffer.clear();
      }
      mutex.unlock();

      time_t t1 = get_ticks();
      time_t toSleep = (time_t)(ds->cachePeriod) - (t1-t0);
      if(toSleep>0)
        usleep(toSleep*1000);
      
    }
       
  }

  //----------------------------------------------------------------------------
  // Return number of milliseconds
  //-----------------------------------------------------------------------------
  time_t ModbusComposerThread::get_ticks() {
      
    if(tickStart < 0 )
      tickStart = time(NULL);
    
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

  }  

} // namespace ModbusComposer_ns
